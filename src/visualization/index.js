import { sum } from "d3-array";
import { RadialGrid, simplifyLargeNumber } from "@lgv/visualization-chart";
import { interpolateNumber, interpolateString } from "d3-interpolate";
import { scaleBand, scaleLinear } from "d3-scale";
import { select } from "d3-selection";
import { arc, stack, stackOffsetExpand } from "d3-shape";
import { transition } from "d3-transition";

import { configuration, configurationLayout } from "../configuration.js";

/**
 * RadialStackedBarChart is a hybrid visualization of a series of stacked bar charts with curved connection paths between related stacked values.
 * @param {array} data - objects where each represents a series in the collection
 * @param {integer} height - artboard height
 * @param {integer} width - artboard width
 */
class RadialStackedBarChart extends RadialGrid {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, name=configuration.name, label=configuration.branding) {

        // initialize inheritance
        super(data, width, height, label, name);

        // update self
        this.bar = null;
        this.barLabel = null;
        this.classBar = `${label}-bar`;
        this.classBarLabel = `${label}-bar-label`;
        this.classBarLabelPartial = `${label}-bar-label-partial`;
        this.classSeries = `${label}-series`;
        this.paddingRing = 0.2;
        this.series = null;

        // need to store old values for transitions between data updates
        this.tweenAngles = {};

        // store initial d3 angle declarations for all data
        Object.keys(this.data).forEach(k => {

            // generate empty objects for each key
            this.tweenAngles[k] = {};

            // loop through series key
            this.layout([this.data[k]]).forEach(d => {

                // update angle
                this.tweenAngles[k][d.key] = {
                    innerRadius: this.calculateOuterRadius(d) + this.ringScale.bandwidth(),
                    outerRadius: this.calculateOuterRadius(d),
                    startAngle: 0,
                    endAngle: Math.PI * 2
                };

            });
        });

    }

    /**
     * Condition data for visualization requirements.
     * @returns An object where keys are series' set label and corresponding values are a object where keys are series' labels and values are each series value.
     */
    get data() {

        // copy source without reference
        let a = typeof structuredClone === "function" ? structuredClone(this.dataSource) : {...this.dataSource};

        // generate data to pass along series id with label/value
        Object.keys(a).forEach(k => Object.keys(a[k]).forEach(kk => a[k][kk] = { value: a[k][kk], series: k}));

        return this.dataSource ? a : {};

    }

    /**
     * Construct layout.
     * @returns A d3 stack layout function.
     */
    get layout() {
        return stack()
            .keys(d => Object.keys(d[0]))
            .value((d,k) => d[k].value)
            .offset(stackOffsetExpand);
    }

    /**
     * Calculate inner radius.
     * @returns A float representing the radius of the innermost circle.
     */
    get radiusInner() {
        return this.radius * 0.25;
    }

    /**
     * Calculate outer radius.
     * @returns A float representing the radius of the outermost circle.
     */
    get radiusOuter() {
        return this.radius;
    }

    /**
     * Construct arc, i.e. curved bar.
     * @returns A d3 arc generator function.
     */
    get arcGenerator() {
        return arc()
            .innerRadius((d,i) => this.calculateOuterRadius(d) + this.ringScale.bandwidth())
            .outerRadius((d,i) => this.calculateOuterRadius(d))
            .startAngle(d =>  this.degreeToRadian(this.seriesScale(d[0][0])))
            .endAngle(d => this.degreeToRadian(this.seriesScale(d[0][1])));
    }

    /**
     * Construct a scale to separate series' values mapping values to degrees in a circle.
     * @returns A d3 scale function.
     */
    get seriesScale() {
        return scaleLinear()
            .domain([0, 1])
            .range([0, 360]);
    }

    /**
     * Construct scale to separate series' sets.
     * @returns A d3 scale function.
     */
    get ringScale() {
        return scaleBand()
            .domain(Object.keys(this.data))
            .rangeRound([this.radiusInner, this.radiusOuter])
            .paddingInner(this.paddingRing);
    }

    /**
     * Determine what the max amount of space a text label will have by default.
     * @returns A float representing max text space available for render.
     */
    get textBounds () {
        return this.ringScale.bandwidth() - (this.ringScale.step() - this.ringScale.bandwidth());
    }

    /**
     * Calculate ring outer radius.
     * @param {object} d - d3.js stack layout data object
     * @returns A float representing the inner radius of a ring.
     */
    calculateOuterRadius(d) {
        return this.ringScale(d[0].data[d.key].series);
    }

    /**
     * Declare bar mouse events.
     */
    configureBarEvents() {
        this.bar
            .on("click", (e,d) => {

                // send event to parent
                this.artboard.dispatch("bar-click", {
                    bubbles: true,
                    detail: {
                        id: d.key,
                        label: d.key,
                        value: d[0].data[d.key].value,
                        valueLabel: simplifyLargeNumber( d[0].data[d.key].value),
                        xy: [e.clientX + (this.unit / 2), e.clientY + (this.unit / 2)]
                    }
                });

            })
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classBar} active`);

                // send event to parent
                this.artboard.dispatch("bar-mouseover", {
                    bubbles: true,
                    detail: {
                        id: d.key,
                        label: d.key,
                        value: d[0].data[d.key].value,
                        valueLabel: simplifyLargeNumber( d[0].data[d.key].value),
                        xy: [e.clientX + (this.unit / 2), e.clientY + (this.unit / 2)]
                    }
                });

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classBar);

                // send event to parent
                this.artboard.dispatch("bar-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Position and minimally style label partials in SVG dom element.
     */
    configureBarLabelPartials() {
        this.barLabelPartial
            .transition().duration(300)
            .attr("class", this.classBarLabelPartial)
            .attr("dx", (d,i,nodes) => {

                let dx = null;

                // only adjust for value partials
                if (i != 0) {

                    // get corresponding label node data
                    let labelData = nodes[i - 1].__data__;

                    // determine text bounds based on arc
                    let angle = this.radianToDegree(this.arcGenerator.endAngle()(d)- this.arcGenerator.startAngle()(d));

                    let tb = angle >= 180 ? this.arcGenerator.outerRadius()(d) : (angle < 10 ? this.letterUnits.M : this.textBounds);

                    // determine how the label will be formatted for render
                    let label = this.determineLabel(i - 1, labelData.key, labelData[0].data[labelData.key].value, tb);

                    // determine label length by character
                    let labelLength = this.determineTextWidth(label);

                    // determine how to center value relative to the label
                    let valueLabel = this.determineLabel(i, d.key, d[0].data[d.key].value, tb).toString();
                    let valueLabelLength = this.determineTextWidth(valueLabel);

                    // update dx
                    dx = ((labelLength / 2) + (valueLabelLength / 2)) * -1;

                }

                return dx;

            })
            .attr("dy", (d,i) => this.calculateLabelDy(i, d[0].data[d.key].value, 50))
            .textTween((d,i) => this.tweenText(d.key, d[0].data[d.key].value, this.determineBounds(d), i));
    }

    /**
     * Position and minimally style bars in SVG dom element.
     */
    configureBarLabels() {
        this.barLabel
            .transition().duration(1000)
            .attr("class", this.classBarLabel)
            .attr("data-label", d => d.key)
            .attr("pointer-events", "none")
            .attr("text-anchor", "middle")
            .attrTween("x", d => {

                // generate tweened angle object & angle interpolators
                const ai = this.tweenAngle(d);

                // store reference to class
                const _self = this;

                return t => {

                    // tween angles and update object
                    ai.angle.startAngle = ai.start(t);
                    ai.angle.endAngle = ai.end(t);

                    return arc().centroid(ai.angle)[0]

                }

            })
            .attrTween("y", d => {

                // generate tweened angle object & angle interpolators
                const ai = this.tweenAngle(d);

                // store reference to class
                const _self = this;

                return t => {

                    // tween angles and update object
                    ai.angle.startAngle = ai.start(t);
                    ai.angle.endAngle = ai.end(t);

                    return arc().centroid(ai.angle)[1]

                }

            })
    }

    /**
     * Position and minimally style bars in SVG dom element.
     */
    configureBars() {
        this.bar
            .transition().duration(1000)
            .attr("class", this.classBar)
            .attr("data-id", d => d.key)
            .attr("data-value", d => d[0].data[d.key].value)
            .attr("data-label", d => d.key)
            .attrTween("d", d => {

                // get values
                let series = d[0].data[d.key].series;

                // stored angles
                let storedEndAngle = this.tweenAngles[series][d.key].endAngle;
                let storedStartAngle = this.tweenAngles[series][d.key].startAngle;

                // new angles, i.e. where transition will end
                let newEndAngle = this.arcGenerator.endAngle()(d);
                let newStartAngle = this.arcGenerator.startAngle()(d);

                // establish interpolators
                const interpolatorEnd = interpolateNumber(storedEndAngle, newEndAngle);
                const interpolatorStart = interpolateNumber(storedStartAngle, newStartAngle);

                // construct d3 arc object minus the start/end angles
                let a = {
                    innerRadius: this.calculateOuterRadius(d) + this.ringScale.bandwidth(),
                    outerRadius: this.calculateOuterRadius(d)
                };

                // store reference to class
                const _self = this;

                return t => {

                    // tween angles and update object
                    a.startAngle = interpolatorStart(t);
                    a.endAngle = interpolatorEnd(t);

                    // update values on class
                    _self.tweenAngles[series][d.key] = a;

                    return arc()(a);

                }

            })
            .attr("fill", "lightgrey");
    }

    /**
    * Position and minimally style bar group in svg dom.
    */
    configureSeries() {
        this.series
            .attr("class", this.classSeries)
            .attr("data-label", d => d);
    }

    /**
     * Determine the available bounds for a given bar.
     * @param {object} d - d3.js stack layout data object
     * @preturns A float which represents the max space available for a label.
     */
    determineBounds(d) {

        // determine text bounds based on arc
        let angle = this.radianToDegree(this.arcGenerator.endAngle()(d)- this.arcGenerator.startAngle()(d));

        return angle >= 180 ? this.arcGenerator.innerRadius()(d) : (angle < 5 ? this.letterUnits.a : this.textBounds);

    }

    /**
     * Generate bar label partials in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateBarLabelPartials(domNode) {
        return domNode
            .selectAll(`.${this.classBarLabelPartial}`)
            .data(d => [d, d])
            .join(
                enter => enter.append("tspan"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct bars in series in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateBars(domNode) {
        return domNode
            .selectAll(`.${this.classBar}`)
            .data(d => this.layout([this.data[d]]))
            .join(
                enter => enter.append("path"),
                update => update,
                exit => exit.transition()
                    .duration(1000)
                    .attr("transform", "scale(0)")
                    .remove()
            );
    }

    /**
     * Construct bar text in series in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateBarLabels(domNode) {
        return domNode
            .selectAll(`.${this.classBarLabel}`)
            .data(d => this.layout([this.data[d]]))
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate visualization.
     */
    generateChart() {

        // generate group for each series
        this.series = this.generateSeries(this.content);
        this.configureSeries();

        // generate bars in each series
        this.bar = this.generateBars(this.series);
        this.configureBars();
        this.configureBarEvents();

        // generate bar labels in each series
        this.barLabel = this.generateBarLabels(this.series);
        this.configureBarLabels();

        // generate label partials so they stack
        this.barLabelPartial = this.generateBarLabelPartials(this.barLabel);
        this.configureBarLabelPartials();

    }

    /**
     * Construct bar group in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateSeries(domNode) {
        return domNode
            .selectAll(`.${this.classSeries}`)
            .data(Object.keys(this.data))
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Tween an angle.
     * @param {object} d - d3.js stack layout data object
     * @returns An object representing a d3.js angle.
     */
    tweenAngle(d) {

        // get values
        let series = d[0].data[d.key].series;

        // stored angles
        let storedEndAngle = this.tweenAngles[series][d.key].endAngle;
        let storedStartAngle = this.tweenAngles[series][d.key].startAngle;

        // new angles, i.e. where transition will end
        let newEndAngle = this.arcGenerator.endAngle()(d);
        let newStartAngle = this.arcGenerator.startAngle()(d);

        // establish interpolators
        const interpolatorEnd = interpolateNumber(storedEndAngle, newEndAngle);
        const interpolatorStart = interpolateNumber(storedStartAngle, newStartAngle);

        // construct d3 arc object minus the start/end angles
        return {
            end: interpolatorEnd,
            angle: {
                innerRadius: this.calculateOuterRadius(d) + this.ringScale.bandwidth(),
                outerRadius: this.calculateOuterRadius(d)
            },
            start: interpolatorStart
        };

    }

};

export { RadialStackedBarChart };
export default RadialStackedBarChart;
