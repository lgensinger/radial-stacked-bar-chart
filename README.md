# Radial Stacked Bar Chart

ES6 d3.js radial stacked bar chart visualization.

## Install

```bash
# install package
npm install @lgv/radial-stacked-bar-chart
```

## Data Format

The following values are the expected input data structure; the keys will be used as the item's id.

```json
{
    "someKey": { "a": 445, "b": 4, "c": 10 },
    "someKey2": { "a": 300, "b": 200, "c": 5 }
}
```

## Use Module

```bash
import { RadialStackedBarChart } from "@lgv/radial-stacked-bar-chart";

// have some data
let data = {
    "someKey": { "a": 445, "b": 4, "c": 10 },
    "someKey2": { "a": 300, "b": 200, "c": 5 }
};

// initialize
const sbc = new RadialStackedBarChart(data);

// render visualization
sbc.render(document.body);
```

## Environment Variables

The following values can be set via environment or passed into the class.

| Name | Type | Description |
| :-- | :-- | :-- |
| `LGV_HEIGHT` | integer | height of artboard |
| `LGV_WIDTH` | integer | width of artboard |

## Events

The following events are dispatched from the svg element. Hover events toggle a class named `active` on the element.

| Target | Name | Event |
| :-- | :-- | :-- |
| bar rect | `bar-click` | on click |
| bar rect | `bar-mouseover` | on hover |
| bar rect | `bar-mousemout` | on un-hover |

## Style

Style is expected to be addressed via css. Any style not met by the visualization module is expected to be added by the importing component.

| Class | Element |
| :-- | :-- |
| `lgv-radial-stacked-bar-chart` | top-level svg element |
| `lgv-content` | content inside artboard inside padding |
| `lgv-bar` | bar chart bar |
| `lgv-bar-label` | bar chart bar label |
| `lgv-series` | bar chart series |

## Actively Develop

```bash
# clone repository
git clone <repo_url>

# update directory context
cd radial-stacked-bar-chart

# run docker container
docker run \
  --rm \
  -it  \
  -v $(pwd):/project \
  -w /project \
  -p 8080:8080 \
  node \
  bash

# FROM INSIDE RUNNING CONTAINER

# install module
npm install .

# run development server
npm run startdocker

# edit src/index.js
# add const sbc = new RadialStackedBarChart(data);
# add sbc.render(document.body);
# replace `data` with whatever data you want to develop with

# view visualization in browser at http://localhost:8080
```
